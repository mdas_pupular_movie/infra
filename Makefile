SHELL := /bin/bash # Use bash syntax
colon := ":"
ns := store

export
init: start_minikube install_addons init_help_repos install_argo install_argo_rollout install_argo_image_updater install_argo_app

start_minikube:
	minikube start --vm=true --cpus 4 --memory 8196 --driver='hyperkit'
#	minikube start --vm=true --cpus 4 --memory 4098
	#minikube start --vm=true --cpus 4 --memory 8098 --nodes 2

install_addons:
	#minikube addons enable metrics-server
	#minikube addons enable dashboard
	minikube addons enable ingress
	minikube addons enable default-storageclass
	minikube addons enable storage-provisioner

init_help_repos:
	#helm repo add grafana https:t//grafana.github.io/helm-charts
	helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
	#helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
	helm repo update

install_argo:
	kubectl create namespace argocd || true
	kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

install_argo_app:
	kubectl apply -f resources/argocd-main.yaml -n argocd

install_argo_rollout:
	kubectl create namespace argo-rollouts || true
	kubectl apply -n argo-rollouts -f https://github.com/argoproj/argo-rollouts/releases/latest/download/install.yaml
	kubectl patch -n argo-rollouts deployment argo-rollouts --patch-file resources/argo-rollout-controller-metrics-patch.yml

install_argo_image_updater:
	kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj-labs/argocd-image-updater/stable/manifests/install.yaml

argo_fwd:
	kubectl -n argocd port-forward svc/argocd-server 8080:443

argo_pwd:
	kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d ; echo

infinispan_pwd:
	kubectl -n ${ns} get secret infinispan-generated-secret -o jsonpath="{.data.password}" | base64 --decode ; echo

grafana_pwd:
	kubectl get secret monitoring -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

install_monitoring_stack:
	kubectl create namespace monitoring || true
	helm install prometheus prometheus-community/prometheus -n monitoring -f resources/values-prometheus.yaml

importer_helm_install:
	helm upgrade --install --wait --create-namespace=true --namespace=$(ns) importer charts/importer -f charts/importer/values.yaml --debug

importer_helm_remove:
	helm uninstall importer --namespace=$(ns)

api_helm_install:
	helm upgrade --install --wait --create-namespace=true --namespace=$(ns) api charts/api -f charts/api/values.yaml --debug

api_helm_remove:
	helm uninstall api --namespace=$(ns)

es_fwd:
	kubectl -n  ${ns} port-forward svc/elasticsearch-master 9200:9200